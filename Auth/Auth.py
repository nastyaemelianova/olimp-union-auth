from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .DAO.init_dao import init as m_init_dao
from .Services.Utils.DBService import DBService
from .Services.init_services import init as m_init_services
from .Views.init_views import init as m_init_views


def init_db(app: FastAPI):
    db_service = DBService(app.state.config)
    db_service.connect()
    app.state.db_service = db_service


def init_services(app: FastAPI):
    m_init_services(app)


def init_views(app):
    m_init_views(app)


def init_dao(app):
    m_init_dao(app)


def create_app(config):
    app = FastAPI()
    origins = ["*"]
    app.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    app.state.config = config
    init_db(app)
    init_dao(app)
    init_services(app)
    init_views(app)

    return app
