from argparse import Namespace

from fastapi import FastAPI

from .UserDAO import UserDAO


def init(app: FastAPI):
    dao = Namespace()
    dao.user = UserDAO(app)
    app.state.dao = dao
