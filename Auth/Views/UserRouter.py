from email_validator import EmailNotValidError
from fastapi import Request, APIRouter, Header
from starlette.responses import JSONResponse
from typing import Optional

from ..Exceptions.AuthException import LoginExists, LoginNotExists, PasswordMismatch, \
    TokenInvalid, TokenExpired
from ..Models import User as UserModel, Responses
from ..Models.FastAPI_helper import FastAPI_M

router = APIRouter()

# Author: Chertkov. V.G.
# Date create: 21.03.2022
# Description: Функция убивает сессию пользователя
@router.delete("/logout")
async def logout(req: Request, Authorization: Optional[str] = Header(None)):
    app: FastAPI_M = req.app
    try:
        is_valid = app.state.services.JWT.decode_token(Authorization)
        return {"app_code": 200, "user": is_valid}

    except Exception as e:
        return {'status': 500, 'msg': e}



# Author: Chertkov. V.G.
# Date create: 18.03.2022
# Description: Функция возвращает объект пользователя по jwt зашитому в поле Authorization заголовка http запроса.
@router.get("/user")
async def getUserByJWT( req: Request, Authorization: Optional[str] = Header(None)):
    app: FastAPI_M = req.app
    # if token is valid
    try:
        payload = app.state.services.JWT.decode_token(Authorization)
        print(payload['sub'])

        # user: Responses.LoginSuccess = await app.state.services.user.login(user)
        user = await app.state.services.user.getUser(payload['sub'])

        return {"user": user}

    except Exception as e:
        return {'status': 500, 'msg': e}





# @router.get("/user", status_code=200, response_model=Responses.JWTDecode)
# async def getUser(jwt: str, req: Request):
#     app: FastAPI_M = req.app
#     try:
#         login_res: Responses.LoginSuccess = await app.state.services.user.login(user)
#     except LoginNotExists as e:
#         return JSONResponse(status_code=403, content={"err_msg": f"Wrong login of password"})
#     except PasswordMismatch as e:
#         return JSONResponse(status_code=403, content={"err_msg": f"Wrong login of password"})
#     # except Exception as e:
#     #     return JSONResponse(status_code=520, content={"err_msg": str(e) or f"Unknown error"})
#     else:
#         return login_res
#     return {'status': 200, 'user': login_res}


#
# services = app.state.services
# jwt:AuthJwt = self.services.JWT
# user:UserService = self.services.User

@router.post("/register", status_code=201, response_model=Responses.RegSuccess,
             responses={400: {"model": Responses.Error}, 520: {"model": Responses.Error}})
async def reg_user(user: UserModel.UserRegModel, req: Request):
    app: FastAPI_M = req.app
    try:
        reg_res: Responses.RegSuccess = await app.state.services.user.create(user)
    except LoginExists as e:
        return JSONResponse(status_code=400, content={"err_msg": str(e) or f"Login is taken"})
    except EmailNotValidError as e:
        return JSONResponse(status_code=400, content={"err_msg": str(e) or f"Email is not valid"})
    # except (AuthDBException, Exception) as e:
    #     return JSONResponse(status_code=520, content={"err_msg": str(e) or f"Unknown error"})
    else:
        return reg_res


@router.post("/login", status_code=200, response_model=Responses.LoginSuccess,
             responses={403: {"model": Responses.Error}})
async def login(user: UserModel.UserLogInModel, req: Request):
    # TODO send email to validate
    app: FastAPI_M = req.app
    try:
        login_res: Responses.LoginSuccess = await app.state.services.user.login(user)
    except LoginNotExists as e:
        #return  { "status_code":403, "msg":"LoginNotExist"}
        return JSONResponse(status_code=200, content={"app_code": 403, "err_msg": f"Wrong login of password"})
    except PasswordMismatch as e:
        return JSONResponse(status_code=200, content={"app_code": 401, "err_msg": f"Wrong login of password"})
    # except Exception as e:
    #     return JSONResponse(status_code=520, content={"err_msg": str(e) or f"Unknown error"})
    else:
        return login_res


@router.post("/decode_jwt", status_code=200, response_model=Responses.JWTDecode)
async def decode(jwt: str, req: Request):
    app: FastAPI_M = req.app
    return app.state.services.JWT.decode_and_validate_any(jwt)


@router.post("/refresh", status_code=200, response_model=Responses.RefSuccess)
async def refresh(login: str, ref_jwt: str, req: Request):
    app: FastAPI_M = req.app
    try:
        ret = await app.state.services.user.refresh(login=login, ref_jwt=ref_jwt)
    except TokenInvalid as e:
        return JSONResponse(status_code=403, content={"err_msg": str(e) or f"Token or login invalid"})
    except TokenExpired as e:
        return JSONResponse(status_code=403, content={"err_msg": str(e) or f"Token expired"})
    else:
        return ret

#TODO forgot password (email) -> "Email sent" or smth. No other data:
# send email with link with uuid

#TODO forgot password callback (uuid):
# replace pwd(md5), DO NOT return tokens

