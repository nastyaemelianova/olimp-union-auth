from email_validator import validate_email, EmailNotValidError
from fastapi import APIRouter

from ..Exceptions.AuthException import LoginExists, AuthDBException, LoginNotExists, PasswordMismatch, \
    TokenInvalid
from ..Models import Responses, User as UserModel
# from ..Models.StateModel import FastAPI_M
from ..Models.FastAPI_helper import FastAPI_M
from ..Models.Responses import Tokens
from ..Services.Utils.DBService import DBService

router = APIRouter()

class User:
    def __init__(self, app):
        self.db_service: DBService = app.state.db_service

        self.app: FastAPI_M = app
        # self.db: pymongo.database.Database = self.db_service.db.get_database(self.db_name)
        # self.coll: pymongo.collection.Collection = self.db.get_collection(self.db_coll)
        # self.DBService = DBService(app.state.config)
        # self.user_dao:UserDAO = app.state.dao.user
        # self.JWT:AuthJwt = app.state.services.JWT

    async def create(self, user: UserModel.UserRegModel) -> Responses.RegSuccess:
        """

        :raise LoginExists:
        :raise EmailNotValidError:
        :raise AuthDBException:
        :param user:
        :return:RegSuccess or raises error
        """
        print("service user create")
        if (await self.app.state.dao.user.get_user_by_login(user.login)) is not None:
            raise LoginExists(f'Login "{user.login}" already exists')

        email = user.email
        try:
            # Validate.
            valid = validate_email(email)
            # Update with the normalized form.
            email = valid.email
        except EmailNotValidError as e:
            # email is not valid, exception message is human-readable
            raise e

        md5_password = self.app.state.services.JWT.md5password(user.password)

        user_m = UserModel.UserDBInModel(
                **{
                        "name": user.name,
                        "surname": user.surname,
                        "email": email,
                        "login": user.login,
                        "md5_password": md5_password,
                        "jwt_wl": []
                })
        user_id = await self.app.state.dao.user.reserve_user()
        user_id = str(user_id)
        is_created = await self.app.state.dao.user.create_reserved(user_id, user_m)

        # self.coll.
        if is_created:
            acc_jwt = self.app.state.services.JWT.generate_access_token({"sub": user_id})
            ref_jwt = self.app.state.services.JWT.generate_refresh_token({"sub": user_id})

            return Responses.RegSuccess(**{
                    "user_id": user_id,
                    "refresh_token": ref_jwt,
                    "access_token": acc_jwt
            })
        else:
            await self.app.state.dao.user.unreserve_user(user_id)
            raise AuthDBException()

    async def get_tokens_and_reset_all_ref(self, user_id: str) -> Tokens:
        user_id = str(user_id)
        acc_jwt = self.app.state.services.JWT.generate_access_token({"sub": user_id})
        ref_jwt = self.app.state.services.JWT.generate_refresh_token({"sub": user_id})
        await self.app.state.dao.user.reset_ref_jwt(user_id, ref_jwt)
        return Tokens(refresh_token=ref_jwt, access_token=acc_jwt)

    async def login(self, user: UserModel.UserLogInModel) -> Responses.LoginSuccess:
        """
        :raise LoginNotExists:
        :raise PasswordMismatch:
        :param user:
        :return:
        """
        print("call method")
        db_user: UserModel.UserDBOutModel = await self.app.state.dao.user.get_user_by_login(user.login)
        if db_user is None:
            raise LoginNotExists(f'User with login {user.login} does not exist')
        inp_md5_password = self.app.state.services.JWT.md5password(user.password)
        if db_user.md5_password != inp_md5_password:
            raise PasswordMismatch("Wrong login or password")

        tokens: Tokens = await self.get_tokens_and_reset_all_ref(db_user.user_id)

        return Responses.LoginSuccess.parse_obj({
                "refresh_token": tokens.refresh_token,
                "access_token": tokens.access_token,
                "user_id": db_user.user_id
        })

    async def getUser(self, id_user: UserModel.IdLogInModel ) -> Responses.LoginSuccess:
        """
        :param id:
        :raise LoginNotExists:
        :raise PasswordMismatch:
        :param user:
        :return:
        """
        user = await self.app.state.dao.user.get_user_by_id(id_user)

        if user is None:
            raise Exception(f'User {id_user} does not exist')
        return user

    async def refresh(self, login: str, ref_jwt: str) -> Responses.RefSuccess:
        """

        :raise TokenInvalid:
        :raise TokenExpired:
        :param login:
        :param ref_jwt:
        :return:
        """

        db_user: UserModel.UserDBOutModel = await self.app.state.dao.user.get_user_by_login(login)
        if db_user.login is None:
            raise TokenInvalid(f"Refresh token does not belong to user {login}")

        ref_dict = self.app.state.services.JWT.decode_and_validate_ref_token(ref_jwt)

        if db_user.user_id != ref_dict["sub"]:
            raise TokenInvalid(f"Refresh token does not belong to user {login}")

        if ref_jwt not in db_user.jwt_wl:
            raise TokenInvalid("Token was refreshed before. Login ")

        new_tokens = self.app.state.services.JWT.validate_ref_and_refresh_acc_token(ref_jwt)

        ret = Responses.RefSuccess(
                **{
                        "refresh_token": new_tokens.refresh_token,
                        "access_token": new_tokens.access_token,
                        "user_id": db_user.user_id
                })
        await self.app.state.dao.user.reset_ref_jwt(db_user.user_id, new_tokens.refresh_token)

        return ret
