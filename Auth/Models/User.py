from typing import List

from pydantic import BaseModel


class UserAuthInModel(BaseModel):
    name: str
    surname: str
    email: str
    login: str
    password: str


class UserLogInModel(BaseModel):
    login: str
    password: str

class IdLogInModel(BaseModel):
    id: str

class UserRegModel(BaseModel):
    name: str
    surname: str
    email: str
    login: str
    password: str


class UserDBInModel(BaseModel):
    name: str
    surname: str
    email: str
    login: str
    md5_password: str
    jwt_wl: List[str] = []  # JWT ref tokens white list


class UserDBOutModel(UserDBInModel):
    user_id: str


class UserOutModel(BaseModel):
    name: str
    surname: str
    email: str
    login: str
