from functools import lru_cache
from Auth.Models.Config import Config

class Settings(Config):
    class Config:
        env_file = ".env"

@lru_cache()
def get_config() -> Settings:
    return Settings()
